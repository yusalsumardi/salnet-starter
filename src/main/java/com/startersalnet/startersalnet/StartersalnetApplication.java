package com.startersalnet.startersalnet;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class StartersalnetApplication {

	public static void main(String[] args) {
		SpringApplication.run(StartersalnetApplication.class, args);
	}

}
