//package com.startersalnet.startersalnet.authentication.configutation.mapper;
//
//import ma.glasnost.orika.MapperFactory;
//import net.rakugakibox.spring.boot.orika.OrikaMapperFactoryConfigurer;
//
//public class UserMapper implements OrikaMapperFactoryConfigurer {
//    @Override
//    public void configure(MapperFactory orikaMapperFactory) {
//        orikaMapperFactory.classMap(BusinessEntity.class, BusinessEntityDto.class)
//                .field(BusinessEntity_.USER+"."+ User_.NAME, "name")
//                .field(BusinessEntity_.USER+"."+ User_.PHONE_NUMBER, "phoneNumber")
//                .field(BusinessEntity_.USER+"."+ User_.EMAIL, "email").byDefault().register();
//
//    }
//}
