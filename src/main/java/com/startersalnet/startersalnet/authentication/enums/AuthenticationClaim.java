package com.startersalnet.startersalnet.authentication.enums;

public enum AuthenticationClaim {
    LOGIN,
    RESET_PASSWORD
}
