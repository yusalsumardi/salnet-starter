package com.startersalnet.startersalnet.authentication.repository;

import com.startersalnet.startersalnet.authentication.entity.UserCredential;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface UserCredentialRepository extends JpaRepository<UserCredential, Long>, JpaSpecificationExecutor {
}
