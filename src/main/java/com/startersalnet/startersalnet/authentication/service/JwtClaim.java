package com.startersalnet.startersalnet.authentication.service;

import com.auth0.jwt.algorithms.Algorithm;
import com.startersalnet.startersalnet.authentication.entity.Token;
import lombok.*;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;
import java.util.UUID;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class JwtClaim {
    private Algorithm signingAlgorithm;

    @NonNull
    private UUID tokenId;

    @NonNull
    private Long userId;

    @NonNull
    private LocalDateTime issuedAt;

    @NonNull
    private LocalDateTime expiresAt;

    @NonNull
    private Token.TokenType type;

    public Date getIssuedAt(){
        return Date.from(this.issuedAt.atZone(ZoneId.systemDefault()).toInstant());
    }

    public Date getExpiresAt() {
        return Date.from(this.expiresAt.atZone(ZoneId.systemDefault()).toInstant());
    }
}
