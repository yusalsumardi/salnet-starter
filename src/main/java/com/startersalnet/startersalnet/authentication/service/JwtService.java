package com.startersalnet.startersalnet.authentication.service;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTCreator;
import com.startersalnet.startersalnet.authentication.enums.AuthenticationClaim;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public class JwtService {
    @Value("${setting.jwt.token.issuer}")
    private String tokenIssuer;
    public static final String TOKEN_TYPE_CLAIM_KEY = "type";
    public static final String TOKEN_USER_ID_CLAIM_KEY = "userId";
    public static final String TOKEN_CLAIM_KEY = "claim";

    public String buildJwtTokenLogin(JwtClaim jwtClaim, Map<String, List<String>> claims) {
        // Initial token builder with details
        JWTCreator.Builder tokenBuilder = JWT
                .create()
                .withIssuer(tokenIssuer)
                .withJWTId(jwtClaim.getTokenId().toString())
                .withIssuedAt(jwtClaim.getIssuedAt())
                .withNotBefore(jwtClaim.getIssuedAt())
                .withExpiresAt(jwtClaim.getExpiresAt())
                .withClaim(TOKEN_TYPE_CLAIM_KEY, jwtClaim.getType().toString())
                .withClaim(TOKEN_USER_ID_CLAIM_KEY, jwtClaim.getUserId())
                .withClaim(TOKEN_CLAIM_KEY, AuthenticationClaim.LOGIN.toString());


        // Load each claim with a mapper
        if (claims != null && !claims.isEmpty()) {
            claims
                    .keySet()
                    .stream()
                    .forEach(
                            thisClaimKey -> tokenBuilder.withClaim(thisClaimKey, claims.get(thisClaimKey))
                    );
        }

        // Sign token with key and return
        return tokenBuilder.sign(jwtClaim.getSigningAlgorithm());
    }
}
