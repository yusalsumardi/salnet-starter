package com.startersalnet.startersalnet.authentication.service;

import com.startersalnet.startersalnet.authentication.dto.UserCredentialDto;
import com.startersalnet.startersalnet.authentication.entity.UserCredential;
import com.startersalnet.startersalnet.authentication.repository.UserCredentialRepository;
import ma.glasnost.orika.MapperFacade;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.context.annotation.RequestScope;

@Service
@RequestScope
public class UserCredentialService {

    @Autowired
    private MapperFacade mapperFacade;

    @Autowired
    private UserCredentialRepository userCredentialRepository;

    public UserCredential createUserCredential(UserCredentialDto userCredential){
        return userCredentialRepository.save(mapperFacade.map(userCredential, UserCredential.class));
    }
}
