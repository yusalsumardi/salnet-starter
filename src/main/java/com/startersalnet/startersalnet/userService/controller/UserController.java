package com.startersalnet.startersalnet.userService.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.startersalnet.startersalnet.userService.dto.UserRegistrationRequestDto;
import com.startersalnet.startersalnet.userService.dto.UserRegistrationResponseDto;
import com.startersalnet.startersalnet.userService.service.UserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/user-service")
@Api("User Controller")
@Slf4j
public class UserController {

    @Autowired
    private UserService userService;

    @PostMapping("/user")
    @ApiOperation(value = "create user and user credential")
    @ApiResponses(
            value = {
                    @ApiResponse(code = 201, message = "Created"),
                    @ApiResponse(code = 400, message = "Bad request due to incorrect payload")
            }
    )
    @ResponseStatus(value = HttpStatus.CREATED)
    public UserRegistrationResponseDto createUser( @RequestBody UserRegistrationRequestDto userRegistrationRequestDto
    ) {
        return userService.createUser(userRegistrationRequestDto);
    }
}
