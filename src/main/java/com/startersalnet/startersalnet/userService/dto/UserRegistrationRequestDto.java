package com.startersalnet.startersalnet.userService.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class UserRegistrationRequestDto {
    private String name;
    private String email;
    private String phoneNumber;
    private String password;
    private String claim;
}
