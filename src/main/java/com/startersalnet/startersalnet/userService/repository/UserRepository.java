package com.startersalnet.startersalnet.userService.repository;

import com.startersalnet.startersalnet.userService.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface UserRepository extends JpaRepository<User, Long>, JpaSpecificationExecutor<User> {
    User findUserByIdAndIsDeletedIsFalse(Long id);
}
