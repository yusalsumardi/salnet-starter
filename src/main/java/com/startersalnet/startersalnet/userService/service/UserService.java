package com.startersalnet.startersalnet.userService.service;

import com.startersalnet.startersalnet.authentication.dto.UserCredentialDto;
import com.startersalnet.startersalnet.authentication.service.UserCredentialService;
import com.startersalnet.startersalnet.userService.dto.UserRegistrationRequestDto;
import com.startersalnet.startersalnet.userService.dto.UserRegistrationResponseDto;
import com.startersalnet.startersalnet.userService.entity.User;
import com.startersalnet.startersalnet.userService.repository.UserRepository;
import ma.glasnost.orika.MapperFacade;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.context.annotation.RequestScope;

@Service
@RequestScope
public class UserService {

    @Autowired
    private MapperFacade mapperFacade;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private UserCredentialService userCredentialService;

    public UserRegistrationResponseDto createUser(UserRegistrationRequestDto requestDto){
        User user = mapperFacade.map(requestDto, User.class);
        User result = userRepository.save(user);

        userCredentialService.createUserCredential(UserCredentialDto.builder()
                        .id(result.getId())
                        .password(requestDto.getPassword())
                .build());

        return mapperFacade.map(result, UserRegistrationResponseDto.class);
    }
}
