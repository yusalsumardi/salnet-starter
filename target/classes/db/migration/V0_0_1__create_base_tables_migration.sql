DROP TABLE IF EXISTS salnet."user" CASCADE;
DROP TABLE IF EXISTS salnet.user_credential CASCADE;

DROP SEQUENCE IF EXISTS salnet.user_id_seq;

CREATE sequence salnet.user_id_seq
    INCREMENT 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    START 1
    CACHE 1;

CREATE TABLE salnet."user" (
                               id bigint NOT NULL default nextval('salnet.user_id_seq'),
                               "name" varchar(255) NOT NULL,
                               phone_number varchar(50) NULL,
                               email varchar(255) NULL,
                               status varchar(20) NOT NULL,
                               is_deleted bool NOT NULL DEFAULT false,
                               is_verified bool NULL DEFAULT false,
                               created_at timestamp without time zone NOT NULL,
                               updated_at timestamp without time zone,
                               CONSTRAINT user_email_key UNIQUE (email),
                               CONSTRAINT user_phone_number_key UNIQUE (phone_number),
                               CONSTRAINT user_pkey PRIMARY KEY (id)
);

CREATE TABLE salnet.user_credential (
                                                id bigint NOT NULL,
                                                pin varchar(500) NULL,
                                                "password" varchar(500) NULL,
                                                created_at timestamp NOT NULL,
                                                updated_at timestamp NULL,
                                                CONSTRAINT user_credential_pkey PRIMARY KEY (id)
);
ALTER TABLE salnet.user_credential ADD CONSTRAINT "fk_usercredential_user_pk " FOREIGN KEY (id) REFERENCES public."user"(id) ON DELETE CASCADE;